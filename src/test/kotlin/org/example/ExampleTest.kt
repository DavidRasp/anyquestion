package org.example

import org.example.nonsealedparameter.IntNsParameter
import org.example.nonsealedparameter.NsParameter
import org.example.nonsealedparameter.StringNsParameter
import org.example.parameter.IntParameter
import org.example.parameter.Parameter
import org.example.parameter.StringParameter
import org.junit.Test

class ExampleTest {
    @Test
    fun testAny() {
        val listBoolean: List<Boolean> = listOf<Boolean>()
        val listAny: List<Any> = listBoolean

        val mapString: Map<String, String> = mapOf()
        val mapAnyAny: Map<Any, Any> = mapString

        val parameterString: Parameter<String> = StringParameter
        val parameterAny: Parameter<Any> = parameterString

        val parameterInt: Parameter<Int> = IntParameter
        val parameterAnyInt: Parameter<Any> = parameterInt

        val nsParameterString: NsParameter<String> = StringNsParameter
        val nsParameterAny: NsParameter<Any> = nsParameterString

        val nsParameterInt: NsParameter<Int> = IntNsParameter
        val nsParameterAnyInt: NsParameter<Any> = nsParameterInt

    }
}